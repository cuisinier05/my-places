import  React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';

import Header from './components/Header/Header';
import Nav from './components/Nav/Nav';
import MyContainer from './components/Main/MyContainer';
import SearchRandonnees from './components/Main/SearchRandonnees';

export default function App() {
  const [buttonText,setButtonText] = useState("Vercors");

  return (
    <PaperProvider>
      <View style={styles.container}>
        <Header textHeader="Mes randos" />
        <Nav setButtonText={setButtonText} buttonText={buttonText} />
        <View>
          <MyContainer textHeader={`Mes randos en ${buttonText}`}  />
        </View>
        <View>
          <MyContainer textHeader="Mes favorites"  />
        </View>
        <SearchRandonnees />
      </View>
    </PaperProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
});
