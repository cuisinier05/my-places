# My Randos
## Description
My Randos est une application codée en React Native. Elle permet à l'utilisateur de consigner les randonnées qu'il a aimé selon les massifs( Vercors, Chartreuse, Belledonne ).
L'idée de base est un "TripAdvisor like" mais plus personnel et qui n'impacte pas les autres usagers. 

## Installation en local

## Step 1
Faire:
 - git clone du projet en local

## Step 2
Dans un terminal en mode administrateur (root), faire:
 - npm install ( Pour intaller les dépendances nécessaires au projet répertoriées dans le fichier package.json )
 - expo doctor ( Pour voir que tout va bien au niveau d'expo en ces temps de confinement ! ;) )

## Step 3
Démarrez l'application en executant "npm start" ou "expo start" dans le terminal

## Step 4
Sur votre téléphone mobile, démarrez l'application "Expo", puis scannez le QR Code que vous avez eu dans le terminal en step 3.
Faites bien attention que le téléphone et l'ordinateur soit sur le même réseau internet pour que l'application puisse se lancer.

## Contact
Pour toute info complémentaire, ou remarque constructive faisant avancer le développement de cette application, contactez moi par mail: alexandre.riera.simplon@gmail.com

### Version 1.0.0

### A venir 
 - D'autres massifs seront ajoutés petit à petit pour assurer un maillage au niveau du département de l'Isère et même de la Savoie et Hautes Alpes.

### Développeur
 - Alexandre RIERA

 ## Difficultés rencontrées
 
 ### Choix des API
 A la base, je voulais faire une application sur des restaurants, hotels et bars que l'utilisateur pouvais noter "pour soi" et ainsi se faire son propre TripAdvisor à lui, mais j'ai eu du mal à trouver les bonnes API ainsi qu'à m'en servir car certaines étaient payantes après coup.

Je donc changé d'idée et je suis partie sur une application regroupant les randonnées locales autours de l'utilisateur.

- Les données brut sont tirées du site de la metropole de Grenoble en format JSON à cette adresse:
https://l.facebook.com/l.php?u=http%3A%2F%2Fdata.metropolegrenoble.fr%2Fckan%2Fdataset%2Fbalades-dans-les-massifs-autour-de-grenoble%3Ffbclid%3DIwAR2xL9q3-6B9tSz1ChCCmvxE_7FhOITApRe8cnCsUWGINtreo9R7--U3H34&h=AT3l8kty_EBXcB-wPKOqUTv51uIZHmtoRQQaCgLag70qPorQoPa_Sur3D4jq2w-kFJJ3y99kYett2vtzWlnkvAjOOudnr2q8VY8qCn0URNsCeXaj8MHsU3WbFvV8PJZ8M2mTb5KKQHc

### Utilisation optimale du tableau d'objets issu de l'import des data

### Perte de beaucoup de temps sur la localisation de la première application

### Pas eu le temps de m'attaquer au front 

Je continuerai à développer cette application car son mode de développement corespond exactement à ce que j'ai besoin pour le développement mobile d'une application que j'avais développé cet été dans le cadre du "Civic Lab" avec la participation de "La turbine" en face de "La Belle Electrique" à Grenoble 