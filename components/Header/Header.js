import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from 'react-native-paper';

const Header = ({textHeader}) =>{
  return(
    <View style={stylesHeader.headerContainer}>
      <Text style={stylesHeader.headerText}>{textHeader}</Text>
    </View>
  )
}
const stylesHeader = StyleSheet.create({
  headerContainer:{
    alignItems:'center',
    paddingTop:30,
    paddingBottom:20,
  },
  headerText:{
    fontSize:25
  }
})
export default Header;