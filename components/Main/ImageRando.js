import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import data from "./randonnees.json";

const ImageRando = (props) => {
    // Le tableau des images
    const tabRandoImage = props.tabRandoImage;
    const setTabRandoImage = props.setTabRandoImage;

    data.map(randoObject => {
        setTabRandoImage({tabRandoImage:[...tabRandoImage, randoObject.img]});
    });
    
    return(
        <View>
            <Image 
            style={styleImage.image} 
            source={ {uri: tabRandoImage[0]} } />
        </View>
    )
}
const styleImage = StyleSheet.create({
    image:{
        width:100,
        height:100,
        marginHorizontal:10
      },
})
export default ImageRando;