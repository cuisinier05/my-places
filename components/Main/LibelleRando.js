import React, { useState } from 'react';
import { Text, View, Image } from 'react-native';
import data from "./randonnees.json";

const LibelleRando = (props) => {
    // Le tableau des libelles
    const tabRandoLibelle = props.tabRandoLibelle;
    const setTabRandoLibelle = props.setTabRandoLibelle;

    data.map(randoObject => {
        setTabRandoLibelle(randoObject.libelle);
    });
    
    return(
        <Text>{tabRandoLibelle}</Text>
    )
}
export default LibelleRando;