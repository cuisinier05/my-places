import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Header from '../Header/Header';
import ViewItem from '../Main/ViewItem';

const MyContainer = ({textHeader}) => {
  return(
    <View style={{ alignItems:"center"}}>
      <Header textHeader={textHeader}/>
      <View>
        <ViewItem />
      </View>
    </View>
  )
}

export default MyContainer;