import React, { useState } from 'react';
import { Text, View } from 'react-native';
import data from "./randonnees.json";

const SearchRandonnees = () => {
    const [tabRandonnees, setTabRandonnees] = useState([]);
    
    const searchRandos = (data) => {
        setTabRandonnees(data);
        console.log(tabRandonnees);
    }
    return(
        <View style={{alignItems:'center',justifyContent:'center',height:100}}>
            <Text onPress={searchRandos}>Les randos en console</Text>
        </View>
    )
}
export default SearchRandonnees;
