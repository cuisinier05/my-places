import React, { useState } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import { AirbnbRating } from 'react-native-elements';
import LibelleRando from './LibelleRando';
import ImageRando from './ImageRando';

const ViewItem = () => {
  // Tableau de tous les libelles des randos
  const [tabRandoLibelle, setTabRandoLibelle] = useState([]);

  // Tableau de toutes les images des randos
  const [tabRandoImage, setTabRandoImage] = useState([]);
  
  return(
    <View style={itemStyle.itemContainer}>
      <ImageRando tabRandoImage={tabRandoImage} setTabRandoImage={setTabRandoImage} />
      <View style={itemStyle.ratingNote}>
        <LibelleRando tabRandoLibelle={tabRandoLibelle} setTabRandoLibelle={setTabRandoLibelle} />
        <AirbnbRating
          count={5}
          reviews={["Mauvaise", "Pas mal", "Bien", "Très bien !", "Inoubliable"]}
          defaultRating={4}
          size={20}
        />
      </View>
    </View>
  )
}

const itemStyle = StyleSheet.create({
  itemContainer:{
    flexDirection:'row'
  },
  ratingNote:{
    paddingLeft:15
  }
})

export default ViewItem;