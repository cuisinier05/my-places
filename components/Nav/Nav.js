import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import {Button} from 'react-native-paper';


const Nav=(props)=>{
  let buttonSetName = props.setButtonText;
  let buttonText = props.buttonText;
  const handleClickButton = (name) => {
    buttonSetName(name);
    console.log(`Vous avez sélectionné: ${name}`);
    return buttonText;
  }

  return(
    <View style={styleNav.navContainer}>
      <Button style={styleNav.navButtons} onPress={()=>{handleClickButton("Vercors")}}>Vercors</Button>
      <Button style={styleNav.navButtons} onPress={()=>{handleClickButton("Chartreuse")}}>Chartreuse</Button>
      <Button style={styleNav.navButtons} onPress={()=>{handleClickButton("Belledonne")}}>Belledonne</Button>
    </View>
  )
}

const styleNav = StyleSheet.create({
  navContainer:{
    flexDirection:'row',
    justifyContent:'space-around',
    paddingTop:30,
  },
  navButtons:{
    color:"black"
  }
})
export default Nav;